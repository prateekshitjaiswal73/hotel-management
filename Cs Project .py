import random

from tabulate import tabulate

import datetime

import mysql.connector as sql , csv

import requests

from geopy.geocoders import OpenCage

from geopy.distance import geodesic


# Main Program Starts i.e ; ___Main___


mycon=sql.connect(host="localhost",user="root",password="12345")

if mycon.is_connected():

    print("*"*73)

    print("\t\t\tConnection Established")

    print("*"*73)

db=input("Enter The Name Of Your Database :~ ")

cursor=mycon.cursor()

query1="create database if not exists {}".format(db)    

cursor.execute(query1)

print("*"*73)

print("                                         Database Created Successfully")

print("*"*73)

cursor=mycon.cursor()

cursor.execute("use "+db)

table=input("Enter The Name Of The Table To Be Created :~ ")

query2="create table if not exists {} (Name varchar(50) , PhoneNo varchar(11), Address varchar(20) , CheckIn date , CheckOut date, RoomNo varchar(23) ,RoomType varchar(20) , RoomCharges varchar(20) , CustomerID varchar(30))".format(table)

cursor.execute(query2)

print("*"*73)

print("                                            Table Created Successfully")

print("*"*73)

#Home

def Home( ):

    print("                             ----WELCOME TO HOTEL ROYAL GRAND----\n")

    print("\t 1- Booking\t2- Payment\t 3- Restaurant\n")

    print("                                 4- Laundry                 5- Records\n")
    print("                                 6- Travel Plan       ")

    print("                                                      0- Exit")

    ch=int(input("Enter Your Choice :~ "))

    if ch==1:

        print(" ")

        Booking( )

    elif ch==2:

        print(" ")

        Payment( )

    elif ch==3:

        print(" ")

        Restaurant( )

    elif ch==4:

        print(" ")

        laundry( )

    elif ch==5:

        print(" ")

        records( )

    elif ch==6:

        print(" ")

        travel_plan( )
        
    else:

        exit()
        


# Setting Date to be Imported From User

def date(d):

    if d[2] >= 2024 and d[2] <= 2025:

        if d[1] != 0 and d[1] <= 12:

            if d[1]== 2 and d[0] != 0 and d[0] <= 31:

                if d[2]%4 == 0 and d[0] <= 29:

                    pass

                elif d[0]<29:

                    pass

                else:

                    print("*"*73)

                    print("\n\t\t\t----Please Try Again ( Invalid Date )----\n")

                    print("*"*73)
                    
                    Booking ( )

            elif d[1] <= 7 and d[1]%2 != 0 and d[0] <= 31:

                pass

            elif d[1] <= 7 and d[1]%2 == 0 and d[0] <= 30 and d[1] != 2:

                pass

            elif d[1] >= 8 and d[1]%2 == 0 and d[0] <= 31: 

                pass

            elif d[1]>=8 and d[1]%2!=0 and d[0]<=30:   

                pass

            else:  

                print("*"*73)

                print("\n\t\t\t----Please Try Again ( Invalid Date )----\n")

                print("*"*73)
                
                Booking ( )

        else:

            print("*"*73)

            print("\n\t\t\t----Please Try Again ( Invalid Date )----\n")

            print("*"*73) 

            Booking ( )

    else:

        print("*"*73)

        print("\n\t\t\t----Please Try Again ( Invalid Date )----\n")

        print("*"*73) 
        
        Booking ( )

#Booking Function
        
def Booking ( ):
    
    print("\t\t\t-----Booking Records-----")

    print(" ")

    while 1: 

            n = str(input("\tName :~ "))

            s1 = str(input("\tPhone Number :~ ")) 

            a = str(input("\tAddress :~ "))

            # Checking That Any Of The Above Information Is Not Empty

            if n!="" and s1!="" and a!="":
                
                break

            else:

                print("*"*73)

                print("\n\t----Name Or Phone Number Or Address Cannot Be Empty----")

                print("\n\t\t\t----(Please Try Again)----")

                print("*"*73)

    print("\n\t\t\t-----Date Format DD/MM/YYYY-----")

    print("                                                                ---Ex. 12/02/2024---")


    checkin=str(input("\tCheck - In :~ "))

    checkin=checkin.split("/")

    checkin[0]=int(checkin[0])

    checkin[1]=int(checkin[1]) 

    checkin[2]=int(checkin[2]) 

    date(checkin) 
           

    checkout=str(input("\tCheck - Out :~ "))
  
    checkout=checkout.split("/") 

    checkout[0]=int(checkout[0]) 

    checkout[1]=int(checkout[1]) 

    checkout[2]=int(checkout[2])

    # Checking Check - Out Date And Check In Date

    if checkout[1]<checkin[1] and checkout[2]<checkin[2]:

        print("*"*73)

        print("\n\t\t---Check- Out Date Must Fall After Check - In Date---")

        print("\t\t\t-----Please Try Again-----")

        print("*"*73)

        Booking ( )

    elif checkout[0]<checkin[0] and checkout[1]<checkin[1] and checkout[2]<=checkin[2]:

        print("*"*73)
        
        print("\n\t\t---Check- Out Date Must Fall After Check - In Date---")

        print("\t\t\t-----Please Try Again-----")

        print("*"*73)
        
        Booking ( ) 

    elif checkout[1]==checkin[1] and checkout[2]>=checkin[2] and checkout[0]<=checkin[0]: 

        print("*"*73)
        
        print("\n\t\t---Check- Out Date Must Fall After Check - In Date---")

        print("\t\t\t-----Please Try Again-----")

        print("*"*73)
        
        Booking ( ) 

    elif checkout[0]>checkin[0] and checkout[1]<checkin[1]and checkout[2]<checkin[2]:

        print("*"*73)
        
        print("\n\t\t---Check- Out Date Must Fall After Check - In Date---")

        print("\t\t\t-----Please Try Again-----")

        print("*"*73)
        
        Booking ( )

    elif checkout[0]>checkin[0] and checkout[1]>=checkin[1] and checkout[2]<checkin[2]:

        print("*"*73)
        
        print("\n\t\t---Check- Out Date Must Fall After Check - In Date---")

        print("\t\t\t-----Please Try Again-----")

        print("*"*73)
         
        Booking ( )

    elif checkout[0]==checkin[0] and checkout[1]>=checkin[1] and checkout[2]>=checkin[2]:

        print("*"*73)
        
        print("\n\t\t---Check- Out Date Must Fall After Check - In Date---")

        print("\t\t\t-----Please Try Again-----")

        print("*"*73)  
        
        Booking ( )

    elif checkout[0]==checkin[0] and checkout[1]<checkin[1]:

        print("*"*73)
        
        print("\n\t\t---Check- Out Date Must Fall After Check - In Date---")

        print("\t\t\t-----Please Try Again-----")

        print("*"*73)
                
        Booking ( )
        
    else:

        pass

    d1 = datetime.date(checkin[2],checkin[1],checkin[0]) 
    d2 = datetime.date(checkout[2],checkout[1],checkout[0])  

    print("\n\t\t----We Have The Following Rooms For You----") 

    print("\t 1. Royal AC") 

    print("\t 2. Luxury AC") 

    print("\t 3. Budget AC") 

    print("\t 4. Elite AC") 

    print(("\t\t\t---Press 0 for Room Prices---")) 
          
    ch=int(input("Enter Your Choice :~ ")) 

    if ch==0: 

        print(" \t1. Royal AC - Rs. 4500") 

        print(" \t2. Luxury AC - Rs. 4000") 

        print(" \t3. Budget AC - Rs. 2500") 

        print(" \t4. Elite AC - Rs. 3500") 

        ch=int(input("Enter Your Choice :~ "))

    if ch==1:

        p="Royal AC"

        rate=4500
     
        print("\t\t\tRoom Type - Royal AC")    

        print("		                          Price - 4500") 

    elif ch==2:

        p="Luxury AC"

        rate=4000
         
        print("\t\t\tRoom Type - Luxury AC") 
        
        print("		                          Price - 4000") 

    elif ch==3:

        p="Budget AC"

        rate=2500
        
        print("\t\t\tRoom Type - Budget AC") 
        
        print("		                          Price - 2500") 

    elif ch==4:

        p="Elite AC"

        rate=3500
        
        print("\t\t\tRoom Type- Elite AC") 
        
        print("		                          Price- 3500") 

    else:

        print("\n\tPlease Try Again ( Wrong Choice )")


    # Generating Room No. and Customer I.D

    roomno=random.randrange(40)

    customerid=n+'@'+str(s1)

    
    # Checking If Alloted Room No. is not Alloted to Anyone Else

    query12="select RoomNo from {}".format(table)

    cursor.execute(query12)

    data=cursor.fetchall()

    for i in data:

        if roomno in i :

            roomno=random.randrange(60)
  
    query3= "select PhoneNo from {} where PhoneNo={}".format(table,s1)
    
    cursor.execute(query3)

    fet=cursor.fetchall()

    r=cursor.rowcount

    if r==0:
   
        print(" ")

        print("*"*73)

        print("\t\t***ROOM BOOKED SUCCESSFULLY***\n")

        query4="insert into {} values('{}',{},'{}','{}','{}',{},'{}',{},'{}')".format(table,n,s1,a,d1,d2,roomno,p,rate,customerid)

        cursor.execute(query4)

        mycon.commit()
    
        query5="insert into Extra_Expense (CustomerID) values ('{}')".format(customerid)

        cursor.execute(query5)

        mycon.commit()

        query6="insert into Total ( CustomerID , Name , PhoneNo , Address , CheckIn , Checkout , RoomNo , RoomType, Room_Charges ) values ('{}','{}',{},'{}','{}','{}',{},'{}','{}')".format(customerid,n,s1,a,d1,d2,roomno,p,rate)

        cursor.execute(query6)

        mycon.commit()

        print("\tRoom Number ~ ",roomno,"                       Customer ID ~ ",customerid) 

        print("*"*73)


    else:
        
        print("*"*73)
                
        print("\n\t----Phone No. Already Exists and Payment Yet To Be Done----")

        print("\t\t\t-----Please Try Again-----")

        print("*"*73)
        

    n=int(input("\nEnter Your Choice ( 0 -Back ) :~ ")) 

    if n==0:

        print("\n")

        Home( ) 

    else:

        exit()

# Restaurant

def Restaurant( ):

    print("                                 ----WELCOME TO HOTEL ROYAL GRAND----")

    print("                                                 ---RESTAURANT SECTION---")

    #Printing Essentials
    
    print("                                                             ---MENU---")

    print()

    #Printing Menu

    import csv

    price={}

    menu_items = {}

    f=open('menu.csv')

    redob=csv.reader(f)

    for i in redob:

        m=(i[2]).strip()

        price[(i[1]).strip()]=m

        menu_items[i[0].strip()] = i[1].strip()

        for j in i:

            print(j+('\t'*2),end=' ')

        print('\n')

    f.close()

    #Taking Inputs From User

    n=int(input('How Many Items To Be Entered :~ '))

    lit=[]

    print()

    lqt=[]

    while n>=0:

        item=input("Enter The Item's Sr No. From The Menu :~ ")

        lit.append(item)

        print()

        qty=input('Enter quantity :~ ')

        lqt.append(qty)

        print()

        if n==1:

            ch=input('Wish to enter more items {Y/N}?')

            if ch=='y' or ch=='Y':

                n+=1

            else:

                break

        n-=1
    
    #Printing Bill

    print('~'*62)

    print('\t'*3+'Hotel Royal Grand')

    print('-'*114)

    print('                                                             Bill')

    print('-'*114)

    total=0

    for i in range(len(lit)):

        print("   Item Names ~ ",menu_items[lit[i]],'\t\t\tQuantity Taken ~ ',lqt[i],end='\t'*2 )

        if menu_items[lit[i]] in price.keys():

            qty=lqt[i]

            cost=int(price.get(menu_items[lit[i]]))*int(qty)

            print(cost)

            total+=cost

            print('\n                                                           Cost ~ ',cost)

            print()

    print()

    print('-'*114)

    print('                                            Total Payable Amount ~ ',total)

    print('-'*114)

    print('                                           Thank You And Visit Again :-)')

    print('~'*62)

    print()

    # Checking If The User Had Booked Room Or Not

    print("    1 ~ Insider ( Booked Room )" , "\t\t2 ~ Outsider ( Not Booked Room )")

    check = int(input("\nEnter Your Choice :~ "))

    if check == 1:

        # Connecting With Main Software

        print("\n")

        print("*"*73)

        ch = input('\nDo You Wish To Pay At Checkout {Y/N} ? ')

        if ch in ('y','Y'):
                  
            junk = input('\nPlease Provide Your Customer ID :~ ')

            mycon=sql.connect(user='root',host='localhost',password='12345',database='hotel')

            if mycon.is_connected():

                cursor=mycon.cursor()

                query26="select CustomerID from {} where CustomerID='{}'".format(table,junk)

                cursor.execute(query26)

                csid=cursor.fetchall()

                rcsid=cursor.rowcount

                if rcsid==0:

                    print("*"*73)

                    print("\t\t\t -----CustomerID Not Found-----")

                    print("                                                          ---Please Try Again---")

                    print("*"*73)

                    Restaurant( )

                else:

                    #Name Of Table Where Extra Expenses Will Be Stored = Extra

                    query24="update Extra_Expense set Restaurant_expense=Restaurant_expense+{} where CustomerID='{}'".format(total,junk)

                    cursor.execute(query24)

                    mycon.commit()

                    query25="update Total set Restaurant_Expense=Restaurant_Expense+{} where CustomerID='{}'".format(total,junk)

                    cursor.execute(query25)

                    mycon.commit()

                    print('\n\t\t\t----Request Proceeded----')

        else:

            print(" \n\tPayment") 

            print(" --------------------------------") 

            print("  \t\tMODE OF PAYMENT") 

            print("  \t1- Credit/Debit Card") 

            print("  \t2- Paytm/PhonePe") 

            print("  \t3- Cash") 

            x = int(input("Enter Your Choice :~ ")) 

            print("\n  Amount :~ ", total) 

            print("\n\t Pay For Royal Grand") 

            print(" Enter Your Choice (Y/N)")

            choice=input("Enter Your Choice")

            if choice == 'y' or choice == 'Y':

                print("Payment Recieved")

                print("Thank You Visit Again :-) ")

            else:

                print("Payment Not Recieved")

                print("Please Try Again")

                Restaurant()

    else:

        name = input("Enter Your Name :~ ")

        address = input("Enter Your Address :~ ")

        Phoneno = int(input("Enter Your Phone Number :~ "))

        print(" \tPayment") 

        print(" --------------------------------") 

        print("  \t\tMODE OF PAYMENT") 

        print("  \t1- Credit/Debit Card") 

        print("  \t2- Paytm/PhonePe") 

        print("  \t3- Cash") 

        x = int(input("Enter Your Choice :~ ")) 

        print("\n  Amount :~ ", total) 

        print("\n\t Pay For Royal Grand") 

        print(" Enter Your Choice (Y/N)")

        choice = input("Enter Your Choice")

        if choice == 'y' or choice == 'Y':

            mycon=sql.connect(user='root',host='localhost',password='12345',database='Hotel')

            cursor=mycon.cursor()

            now = datetime.datetime.now()

            current = now.strftime("%Y-%m-%d")

            time = now.strftime("%H:%M:%S")

            query13="insert into restaurant_outsider values ('{}','{}',{},{},'{}','{}')".format(name,address,Phoneno,total,current,time)

            cursor.execute(query13)

            mycon.commit()

            print("Payment Recieved")

            print("Thank You Visit Again :-) ")

            

        else:

            print("Payment Not Recieved")

            print("Please Try Again")

            Restaurant()

    n = int(input("\nEnter Your Choice ( 0 -Back ) :~ ")) 

    if n == 0:

        print("\n")

        Home( ) 

    else:

        exit()

    
    
# Laundry Function

def laundry( ):

    print("Welcome to laundary column")

    # Printing the menu for laundry function

    with open('Laundary.csv','r') as f:

        r = csv.reader(f)

        for i in r:

            print(i)
            
    # Connecting With Main Software

    mycon=sql.connect(user='root',host='localhost',password='12345',database='Hotel')

    cursor=mycon.cursor()
    
    while True:

        cus = input("Enter Your Customer ID :~ ")
        
        cursor.execute("select CustomerID from {} where CustomerID='{}'".format(table,cus))

        a = cursor.fetchall()

        r = cursor.rowcount

        # Checking the validity of the customer
        
        if r == 0:

            print("Enter a valid CustomerID !!!")

        else:

            break

    p = 0

    x = 'Y'

    while x == 'Y':

        # Taking the order from user

        ch = int(input("Enter the SNo. of Cloth type here --> "))

        if ch == 1:

            ns = int(input("Enter the number of T-Shirt/Shirt you want to give --> "))

            p += (ns*90)

        elif ch == 2:

            np = int(input("Enter the number of Pant/Jeans you want to give --> "))

            p += (np*130)

        elif ch == 3:

            nsa = int(input("Enter the number of Saree you want to give --> "))

            p += (nsa*240)

        elif ch == 4:

            nsu = int(input("Enter the number of Suit you want to give --> "))

            p += (nsu*340)

        elif ch == 5:

            ng = int(input("Enter the number of Gown you want to give --> "))

            p += (ng*220)

        elif ch == 6:

            nsw = int(input("Enter the number of Sweater/Pullover you want to give --> "))

            p += (nsw*200)

        elif ch == 7:

            nl = int(input("Enter the number of Lehenga you want to give --> "))

            p += (nl*760)

        elif ch == 8:

            nsl = int(input("Enter the number of Salwar you want to give --> "))

            p += (nsl*330)

        elif ch == 9:

            nlj = int(input("Enter the number of Leather Jacket you want to give --> "))

            p += (nlj*460)

        elif ch == 10:

            nq = int(input("Enter the number of Quilt you want to give --> "))

            p += (nq*300)

        else:

            print("\n\t Please enter a valid Number !!! ")

            continue

        x = input("\n Do you wish to give more Clothes ? (Y/n) --> ")

        if x == 'n' or x == 'N':

            # Asking for the bill payment

            ch = input('\n Do You Wish To Pay At Checkout ??{Y/N} :~ ')

            if ch in ('y','Y'):
                  
                junk = input('\n Please Provide Your Customer ID :~ ')

                if mycon.is_connected():

                    cursor=mycon.cursor()

                    query26="select CustomerID from {} where CustomerID='{}'".format(table,junk)

                    cursor.execute(query26)

                    csid=cursor.fetchall()

                    rcsid=cursor.rowcount

                    # Checking the validity of the customer
                    
                    if rcsid == 0:

                        print("\t\t Invalid CustomerID ( Please Try Again )\n")

                        laundry( )

                    else:

                        # Name Of Table Where Extra Expenses Will Be Stored = Extra

                        query28="update Extra_Expense set Laundry_Expense=Laundry_Expense+{} where CustomerID='{}'".format(p,junk)

                        cursor.execute(query28)

                        mycon.commit()

                        query29="update Total set Laundry_Expense=Laundry_Expense+{} where CustomerID='{}'".format(p,junk)

                        cursor.execute(query29)

                        mycon.commit()

                        print('\t\t Request Proceeded')

            else:

                # Printing Bill

                print(" \t Payment") 

                print("-"*130) 

                print("  \t\t MODE OF PAYMENT") 

                print("  \t1 - Credit/Debit Card") 

                print("  \t2 - Paytm/PhonePe") 

                print("  \t3 - Cash") 

                x = int(input("\n Enter Your Choice :~ ")) 

                print("\n  Amount :~ ", p) 

                print("\n\t Pay For Royal Grand")

                print("\n Reply in (Y/N) ")

                choice = input("\n Enter Your Choice :~ ")

                # Checking if the customer has payed the bill or not

                if choice == 'y' or choice == 'Y':

                    print("Payment Recieved")

                    print("\t\t Thank You Visit Again :-) ")

                else:

                    # If he/she has not payed 

                    print("Payment Not Recieved")

                    print("Please Try Again")

                    laundry( )
    
# Payment Function

def Payment( ):

    ph=int(input("\tPlease Enter Your Registered Mobile Number :~ "))

    query5="select PhoneNo from {} where PhoneNo={}".format(table,ph)

    cursor.execute(query5)

    f=cursor.fetchall()

    row=cursor.rowcount

    if row==0:

        print("\n\t\tPlease Try Again (Can't Find Records )")

        Payment( )

    else:

        query6="select Name from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query6)

        z=cursor.fetchall()

        newz=z[0][0]

        query7="select PhoneNo from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query7)

        y=cursor.fetchall()

        newy=y[0][0]

        query8="select Address from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query8)

        m=cursor.fetchall()

        newm=m[0][0]

        query9="select Checkin from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query9)

        w=cursor.fetchall()

        neww=w[0][0]

        query10="select Checkout from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query10)

        v=cursor.fetchall()

        newv=v[0][0]

        query11="select RoomType from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query11)

        u=cursor.fetchall()

        newu=u[0][0]

        day =newv- neww

        newday = day.days

        query14="select RoomCharges from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query14)

        g=cursor.fetchall()

        newg=g[0][0]

        query16="select RoomNo from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query16)

        rno=cursor.fetchall()

        newrno=rno[0][0]

        query17="select CustomerID from {} where PhoneNo={}".format(table,ph)

        cursor.execute(query17)

        cid=cursor.fetchall()

        newcid=cid[0][0]

        query18="select Restaurant_Expense from Extra_Expense where CustomerID='{}'".format(newcid)

        cursor.execute(query18)

        re=cursor.fetchall()

        newre=re[0][0]

        query19="select Laundry_Expense from Extra_Expense where CustomerID='{}'".format(newcid)

        cursor.execute(query19)

        le=cursor.fetchall()

        newle=le[0][0]
        
        print(" \tPayment") 

        print(" --------------------------------") 

        print("  \t\tMODE OF PAYMENT") 

        print("  \t1- Credit/Debit Card") 

        print("  \t2- Paytm/PhonePe") 

        print("  \t3- Cash") 

        x=int(input("Enter Your Choice : "))

        ch="y"

        if ch=="Y" or ch=="y":
            query22="update Total set Room_Charges=Room_Charges+{} where PhoneNo='{}'".format(int(newday)*int(g[0][0]),ph)

            cursor.execute(query22)

            mycon.commit()

            query23="update Total set Total_Amount =(select sum(Room_Charges + Restaurant_Expense + Laundry_Expense)) where PhoneNo = {}".format(ph)

            cursor.execute(query23)

            mycon.commit()

            query27="select Total_Amount from Total where PhoneNo={}".format(ph)

            cursor.execute(query27)

            ta=cursor.fetchall()

            newta=ta[0][0]
 
            print("\n\n -----------------------------------------------------------") 

            print(" \t\tHotel Royal Grand") 

            print(" ------------------------------------------------------------------") 

            print(" \t\tBill") 

            print(" ------------------------------------------------------------------") 

            print(" Name :~ ",newz,"\t\n Phone Number :~ ",newy,"\t\n Address :~ ",newm,"\t") 

            print("\n Check-In :~ ",neww,"\t\n Check-Out :~ ",newv,"\t") 

            print("\n Room Type :~ ",newu,"\t\n Room Charges :~ ",int(newday)*int(g[0][0])) 

            print(" ------------------------------------------------------------------") 

            print("\n Total Amount :~ ",newta) 

            print("\t ------------------------------------------------------------------") 

            print(" \t\tThank You") 

            print(" \t\tVisit Again :)") 

            print(" ---------------------------------------------------------------\n")
            
            query20_check = "SELECT * FROM Checkout WHERE CustomerID = '{}'".format(newcid)

            cursor.execute(query20_check)

            result = cursor.fetchall()

            if len(result) == 0:
                query20="insert into Checkout values('{}','{}',{},'{}','{}','{}',{},'{}',{},{},{})".format(newcid,newz,newy,newm,neww,newv,newrno,newu,newg,newre,newle)

                cursor.execute(query20)

                mycon.commit()
            

            query21="delete from Grand where PhoneNo={}".format(ph)

            cursor.execute(query21)

            mycon.commit()
            query_delete = "DELETE FROM Total WHERE PhoneNo = {}".format(ph)
            cursor.execute(query_delete)
            mycon.commit()

            query_delete = "DELETE FROM Extra_Expense WHERE CustomerID IN (SELECT CustomerID FROM Total WHERE PhoneNo = {})".format(ph)
            cursor.execute(query_delete)
            mycon.commit()

            # query_delete = "DELETE FROM Booking WHERE PhoneNo = {}".format(ph)
            # cursor.execute(query_delete)
            # mycon.commit()

            query_delete = "DELETE FROM Checkout WHERE PhoneNo = {}".format(ph)
            cursor.execute(query_delete)
            mycon.commit()

            query_delete = "DELETE FROM {} WHERE PhoneNo = {}".format(table, ph)
            cursor.execute(query_delete)
            mycon.commit()

        else:

            print("\t\tPayment Not Done Please Try Again")

            Payment( )

            

    n = int(input("\nEnter Your Choice ( 0 -Back ) :-")) 

    if n == 0:

        print("\n")

        Home( ) 

    else: 

        exit()



def records():
    
    while True:
        
        ch = int(input('For current customers enter 1 \nFor previous customers enter 2 \nFor specific record press 3 \nFor customers not of hotel 4 \n --->'))
        
        if ch == 1:

            c = input("Do you want to see all details (Y/n) --> ")

            if c == 'Y':

                cursor.execute('select * from total')

            else:
            
                cursor.execute('select * from {}'.format(table))
            
            data = cursor.fetchall()
            
            print('printing records of current customers----------')
            
            for i in data:
                
                print(i)

            print('\n')

            print('-'*130)

            Home()

            
        elif ch == 2:
            
            cursor.execute('select * from Checkout')
            
            data = cursor.fetchall()
            
            print('record of previous customer----------------')
            
            for i in data:
                
                print(i)
                
            print('\n')

            print('-'*130)

            Home()

        
        elif ch == 3:
            
            name = input('enter name of customer')
            
            contact = int(input('enter contact number'))
            
            custid = name+'@'+str(contact)
            
            q2 = 'select * from Checkout where CustomerID="{}"'.format(custid)

            ###################exception ki roots yaha se shuru
            
            cursor.execute(q2)

            ############table name is kal
            
            data = cursor.fetchall()
            
            if len(data) == 0:
                
                print('EMPTY SET!!!!  NO RECORD FOUND')
                
            else:
                
                print(data)
                
            print('\n')

            print('-'*130)

            Home()

        

        elif ch == 4:

            cursor.execute('select *from restaurant_outsider')
            
            data = cursor.fetchall()
            
            print('Printing records of outsiders , who used the Restaurant facilities----------')
            
            for i in data:
                
                print(i)
                
            print('\n')

            print('-'*130)

            Home()


        else:
            
            print('INVALID INPUT')
            
            pass
    
            records()




def travel_plan():
    while True:
        location = input("Enter your location: ")
        budget = input("Enter your budget (low, medium, high): ")

        api_key = "349eabe1c2ee40feb1996137ee46f575"  # Replace with your OpenCage API key
        geocoder = OpenCage(api_key)

        url = f"https://api.opencagedata.com/geocode/v1/json?q={location}&key={api_key}"
        response = requests.get(url)
        data = response.json()

        if "results" not in data or len(data["results"]) == 0:
            print("Location not found. Please try again.")
            continue

        lat = data["results"][0]["geometry"]["lat"]
        lng = data["results"][0]["geometry"]["lng"]

        # Use the OpenCage Geocoder API to get nearby tourist attractions
        attractions_url = f"https://api.opencagedata.com/geocode/v1/json?q=tourist+attractions+near+{location}&key={api_key}"
        attractions_response = requests.get(attractions_url)
        attractions_data = attractions_response.json()

        if "results" not in attractions_data or len(attractions_data["results"]) == 0:
            print("No tourist attractions found near the location.")
            continue

        attractions = []
        for result in attractions_data["results"]:
            description = result["annotations"].get("what", "No description available")
            attractions.append({
                "name": result["formatted"],  # Use the formatted field for the attraction name
                "description": description,
                "lat": result["geometry"]["lat"],
                "lng": result["geometry"]["lng"]
            })

        if budget == 'low':
            num_attractions = min(3, len(attractions))  # Select up to 3 attractions
        elif budget == 'medium':
            num_attractions = min(5, len(attractions))  # Select up to 5 attractions
        else:
            num_attractions = min(7, len(attractions))  # Select up to 7 attractions

        selected_attractions = random.sample(attractions, num_attractions)

        travel_plan = []

        for i in range(num_attractions - 1):
            origin = (selected_attractions[i]["lat"], selected_attractions[i]["lng"])
            destination = (selected_attractions[i+1]["lat"], selected_attractions[i+1]["lng"])

            # Calculate the distance between two points using the Haversine formula
            distance = geodesic(origin, destination).miles

            # Calculate the estimated travel time between two points (assuming a walking speed of 3 miles per hour)
            time = distance / 3 * 60  # Convert hours to minutes

            # Get the directions between two points (assuming a straight line)
            directions = f"Walk from {selected_attractions[i]['name']} to {selected_attractions[i+1]['name']}."

            travel_plan.append({
                "from": selected_attractions[i]["name"],
                "to": selected_attractions[i+1]["name"],
                "distance": distance,
                "time": time,
                "directions": directions
            })

        print("Your travel plan:")
        for plan in travel_plan:
            print(f"Walk from {plan['from']} to {plan['to']}. Distance: {plan['distance']} miles, Time: {plan['time']} minutes. Directions: {plan['directions']}")

        break


#Main Function
 
Home ( )




    
            
    
        
